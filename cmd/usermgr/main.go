package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/cratermoon/pbexample/internal/pkg/userdb"
)


func main() {
	dbFname := flag.String("f", "db.json", "json user db")
	userId := flag.String("u", "ringo@beatles.com", "user id to look up")
	flag.Parse();
	f, err := os.Open(*dbFname)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	db, err := userdb.LoadUserDb(f)
	if err != nil {
		log.Fatal(err)
	}
	u, err := db.LookupUser(*userId)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(u)
}
