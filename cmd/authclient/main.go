package main

import (
	"flag"
	"log"
	"os"
	"text/template"
	"time"

	"github.com/golang/protobuf/ptypes"
	"gitlab.com/cratermoon/pbexample/internal/pkg/authsession"
	"gitlab.com/cratermoon/pbexample/internal/pkg/frontend"
)

const (
	port = ":50051"
)

const userResponse = `{{ .SessionResponse.User.UserName }} authenticated at {{ .Created }}, session expires at {{ .Expires }} ({{ .Remaining }} from now)
Hello {{ .SessionResponse.User.UserGivenName }} {{ .SessionResponse.User.UserSurname }} ({{ .SessionResponse.User.UserCommonName }})
Your email is {{ .SessionResponse.User.UserEmail }}
Your group(s) are: {{ range $idx, $group := .SessionResponse.User.Groups }} <{{- $group -}}> {{ end }}
Response ID: {{ .SessionResponse.Id }} NameID: {{ .SessionResponse.NameId }} SessionIndex: {{ .SessionResponse.Index }}
`

func main() {
	uPtr := flag.String("user", "", "username (required)")
	pPtr := flag.String("pass", "hunter2", "password (default: *******)")
	hPtr := flag.String("host", "localhost", "hostname (default: localhost)")
	flag.Parse()

	if *uPtr == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	s, err := frontend.Authenticate(*uPtr, *pPtr, *hPtr)
	if err != nil {
		log.Fatalf("could not authenticate %s: %v", *uPtr, err)
	}
	create, err := ptypes.Timestamp(s.CreateTime)
	expires, err := ptypes.Timestamp(s.ExpireTime)
	type Response struct {
		SessionResponse             *authsession.Session
		Created, Expires, Remaining string
	}
	t := template.Must(template.New("userResponse").Parse(userResponse))
	err = t.Execute(os.Stdout, Response{s, create.Format(time.ANSIC), expires.Format(time.ANSIC), expires.Sub(create).String()})
	if err != nil {
		log.Println("executing template:", err)
	}

}
