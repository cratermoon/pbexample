package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/cratermoon/pbexample/internal/pkg/backend"
	"gitlab.com/cratermoon/pbexample/internal/pkg/userdb"
)

func main() {
	fmt.Println("Hello, world!")
	if len(os.Args) < 2 {
		fmt.Println("Usage: authserver <filename>")
		os.Exit(1)
	}
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	db, err := userdb.LoadUserDb(f)
	if err != nil {
		log.Fatal(err)
	}
	b := backend.With(db)

	if err := b.Start(); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
