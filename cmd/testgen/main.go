package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

type User struct {
        Groups               []string `protobuf:"bytes,6,rep,name=groups,proto3" json:"groups,omitempty"`
        UserName             string   `protobuf:"bytes,7,opt,name=userName,proto3" json:"userName,omitempty"`
        UserEmail            string   `protobuf:"bytes,8,opt,name=userEmail,proto3" json:"userEmail,omitempty"`
        UserCommonName       string   `protobuf:"bytes,9,opt,name=userCommonName,proto3" json:"userCommonName,omitempty"`
        UserSurname          string   `protobuf:"bytes,10,opt,name=userSurname,proto3" json:"userSurname,omitempty"`
        UserGivenName        string   `protobuf:"bytes,11,opt,name=userGivenName,proto3" json:"userGivenName,omitempty"`
}

func readFile(fname string) []byte {
	f, err := ioutil.ReadFile(fname)
	if err != nil {
		panic(err)
	}
	return f	
}

func readCsvFile(r io.Reader) [][]string{
	csv := csv.NewReader(r)
	csv.LazyQuotes = true
	records,err := csv.ReadAll()
	if err != nil {
		panic(err)
	}
	return records
}
func split(d []byte) []string {
	return strings.Split(string(d), "\n")
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano() ^ int64(os.Getpid()))
}

func randElement(list []string) string {
	return strings.TrimSpace(list[rand.Intn(len(list) - 1)])
}

func main() {

	// first names
        // last names
	// domains
        // group

	records := readCsvFile(os.Stdin)
	words := split(readFile("/usr/share/dict/web2"))

	groups := []string{"user", "staff", "admin", "student"}

	users := make(map[string]User,124)

	for _, name := range records {
		sname, cname := name[0], name[1]
		uname := strings.ToLower(string(cname[0]) + sname)
		u := User {
			Groups:[]string{randElement(groups), uname},
			UserEmail:uname + "@" + randElement(words) + ".com",
			UserName:uname,
			UserCommonName:cname + " " + sname,
			UserGivenName:cname,
			UserSurname:sname,
		}
		users[u.UserEmail] = u
	}
	json, err := json.MarshalIndent(users, "", "    ")
	if err != nil {
		panic(err)
	}

	fmt.Println(string(json))
}
