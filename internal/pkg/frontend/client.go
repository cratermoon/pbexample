package frontend

import (
	"context"
	"log"
	"time"

	"gitlab.com/cratermoon/pbexample/internal/pkg/authsession"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

// Authenticate creates a session for a user with the given password at the given hostname
func Authenticate(username string, password string, hostname string) (*authsession.Session, error) {
	conn, err := grpc.Dial(hostname+port, grpc.WithInsecure())
	if err != nil {
		log.Printf("did not connect: %v", err)
		return nil, err
	}
	defer conn.Close()
	c := authsession.NewLoginClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	return c.Authenticate(ctx, &authsession.Credentials{Username: username, Password: password})
}
