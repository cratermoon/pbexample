package backend

import (
	"context"
	"log"
	"net"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	"gitlab.com/cratermoon/pbexample/internal/pkg/authsession"
	"gitlab.com/cratermoon/pbexample/internal/pkg/userdb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	port = ":50051"
)

// AuthServer for Session authentication
type AuthServer struct {
	userDB userdb.UserDB
}

// With creates an AuthServer with the given user db
func With(db userdb.UserDB) AuthServer {
	return AuthServer{db}
}

// Start runs the authserver
func (as *AuthServer) Start() error {

	lis, err := net.Listen("tcp", port)
	if err != nil {
		return err
	}
	s := grpc.NewServer()
	authsession.RegisterLoginServer(s, as)
	// Register reflection service on gRPC server.
	reflection.Register(s)
	return s.Serve(lis)

}

// Authenticate retrieves the user from the DB and creates the session
func (as *AuthServer) Authenticate(ctx context.Context, creds *authsession.Credentials) (*authsession.Session, error) {
	log.Printf("Got auth request for %s", creds.Username)
	now := time.Now()
	expires := now.Add(time.Minute * 30)

	ct, err := ptypes.TimestampProto(now)
	if err != nil {
		return nil, err
	}
	et, err := ptypes.TimestampProto(expires)
	if err != nil {
		return nil, err
	}
	user, err := as.userDB.LookupUser(creds.Username)
	if err != nil {
		return nil, err
	}
	sess := &authsession.Session{
		Id:         "_" + strings.Replace(uuid.New().String(), "-", "", -1),
		CreateTime: ct,
		ExpireTime: et,
		Index:      "_" + strings.Replace(uuid.New().String(), "-", "", -1),
		NameId:     creds.Username,
		User:       &user,
	}
	log.Println(sess)
	return sess, nil
}
