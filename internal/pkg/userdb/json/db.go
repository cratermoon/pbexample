package json

import (
	"encoding/json"
	"errors"
	"io"
	"log"

	"gitlab.com/cratermoon/pbexample/internal/pkg/authsession"
)

// JsonUserDB stores users in a json file
type JsonUserDB struct {
	users map[string]authsession.User
}

// LookupUser finds a given user by id
func (db JsonUserDB) LookupUser(id string) (authsession.User, error) {
	u, ok := db.users[id]
	if !ok {
		return u, errors.New("No such user " + id)
	}
	return db.users[id], nil
}

func Init(r io.Reader) (db JsonUserDB, err error) {
	dec := json.NewDecoder(r)
	usrs := make(map[string]authsession.User)
	if err := dec.Decode(&usrs); err != nil {
		return JsonUserDB{}, err
	}
	log.Printf("Loaded %d users\n", len(usrs))
	return JsonUserDB{usrs}, nil
}
