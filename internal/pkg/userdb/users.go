package userdb

import (
	"io"

	"gitlab.com/cratermoon/pbexample/internal/pkg/authsession"
	"gitlab.com/cratermoon/pbexample/internal/pkg/userdb/json"
)

// UserDB stores users
type UserDB interface {
    LookupUser(id string) (authsession.User, error)
}

// LoadUserDb initializes the user db
func LoadUserDb(r io.Reader) (db UserDB, err error) {
	return json.Init(r)
}
