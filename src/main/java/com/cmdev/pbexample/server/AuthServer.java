package com.cmdev.pbexample.server;

import java.io.IOException;
import java.io.Reader;
import java.util.logging.Logger;
import java.util.Map;
import java.util.UUID;
import java.time.Duration;
import java.time.Instant;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import com.google.protobuf.Timestamp;

import com.cmdev.pbexample.session.*;
import com.cmdev.pbexample.userdb.*;
import com.cmdev.pbexample.userdb.User;

public class AuthServer {
	private static final Logger logger = Logger.getLogger(AuthServer.class.getName());
    private Server server;
    private int port;
    private UserDB db;

    public AuthServer(UserDB db, int port) {
        this.port = port;
        this.db = db;
    }

	public void Start() throws IOException {
		server = ServerBuilder.forPort(port)
			.addService(new LoginImpl())
			.build()
			.start();
		logger.info("Server started, listening on " + port);
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				// Use stderr here since the logger may have been reset by its JVM shutdown hook.
				System.err.println("*** shutting down gRPC server since JVM is shutting down");
				AuthServer.this.stop();
				System.err.println("*** server shut down");
				}));
	}

	private void stop() {
		if (server != null) {
			server.shutdown();
		}
	}

	public void BlockUntilShutdown() throws InterruptedException {
		if (server != null) {
			server.awaitTermination();
		}
	}


	class LoginImpl extends LoginGrpc.LoginImplBase {
		@Override
			public void authenticate(Credentials creds, StreamObserver<Session> responseObserver) {
 				String username = creds.getUsername();
				logger.info("Got authenticate message for user " + username);
                User u = db.lookupUser(username);
				Session sess = buildSession(u);
				responseObserver.onNext(sess);
				responseObserver.onCompleted();
			}

		private Session buildSession(User u) {
			Instant now = Instant.now();
			Instant expires = now.plus(Duration.ofMinutes(30));
			Timestamp currentTime = Timestamp.newBuilder().setSeconds(now.getEpochSecond())
				.setNanos(now.getNano()).build();
			Timestamp expireTime = Timestamp.newBuilder().setSeconds(expires.getEpochSecond())
				.setNanos(expires.getNano()).build();
			return Session.newBuilder()
				.setId("_"+UUID.randomUUID().toString())
				.setCreateTime(currentTime)
				.setExpireTime(expireTime)
				.setIndex("_"+UUID.randomUUID().toString().replaceAll("-", ""))
				.setNameId(u.userName)
				.setUser(buildUser(u))
				.build();
		}

		private com.cmdev.pbexample.session.User buildUser(User u) {
			return com.cmdev.pbexample.session.User.newBuilder()
			.addGroups(u.groups.toString())
			.setUserName(u.userName)
			.setUserEmail(u.userEmail)
			.setUserCommonName(u.userCommonName)
			.setUserSurname(u.userSurname)
			.setUserGivenName(u.userGivenName)
			.build();

		}
	}
}

