package com.cmdev.pbexample.userdb;

import com.cmdev.pbexample.userdb.User;

public interface UserDB {
    public User lookupUser(String username);
}