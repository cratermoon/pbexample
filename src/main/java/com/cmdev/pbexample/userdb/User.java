package com.cmdev.pbexample.userdb;

import java.util.List;

public class User {
    public List<String> groups;
    public  String userName;
    public String userSurname;
    public String userEmail;
    public String userCommonName;
    public String userGivenName;
}
