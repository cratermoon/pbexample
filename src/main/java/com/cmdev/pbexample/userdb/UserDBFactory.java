package com.cmdev.pbexample.userdb;

import java.io.IOException;
import java.io.Reader;

public class UserDBFactory {
    public static UserDB build(Reader r) throws IOException {
        return new JsonUserDB(r);
    }
}