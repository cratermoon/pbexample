package com.cmdev.pbexample.userdb;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Reader;
import java.util.Map;


class JsonUserDB  implements UserDB{

    private Map<String, User> userDB;

    JsonUserDB(Reader r) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        userDB = mapper.readValue(r, new TypeReference<Map<String, User>>() { });
    }

    public User lookupUser(String username) {
        return userDB.get(username);
    }
}