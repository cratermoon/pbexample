package com.cmdev.pbexample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

import com.cmdev.pbexample.server.*;
import com.cmdev.pbexample.userdb.*;

public class ServerRunner {
	private static final Logger logger = Logger.getLogger(ServerRunner.class.getName());
	private static int port = 50051;


	public static void main(String[] args) throws IOException, InterruptedException {
		if (args.length < 1) {
			System.err.println("Usage: ServerRunner <db file name>");
			System.exit(1);
		}
		Reader r;
		r = Files.newBufferedReader(Paths.get(args[0]));
		UserDB db;
		db = UserDBFactory.build(r);
		final AuthServer server = new AuthServer(db, port);
		server.Start();
		server.BlockUntilShutdown();
	}


}

