package com.cmdev.pbexample;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import com.cmdev.pbexample.session.*;

public class AuthClient {
	private static final Logger logger = Logger.getLogger(AuthClient.class.getName());

	private final ManagedChannel channel;
	private final LoginGrpc.LoginBlockingStub blockingStub;

	public AuthClient(String host, int port) {
		this(ManagedChannelBuilder.forAddress(host, port)
				.usePlaintext()
				.build());
	}

	public AuthClient(ManagedChannel channel) {
		this.channel = channel;
		blockingStub = LoginGrpc.newBlockingStub(channel);
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	public void login(String username, String password) {
		Credentials creds = Credentials.newBuilder()
			.setUsername(username)
			.setPassword(password)
			.build();

		Session sess = blockingStub.authenticate(creds);
		logger.info(sess.toString());
	}

	public static void main(String[] args) throws Exception {
		String username;
		String password;
		AuthClient client = new AuthClient("localhost", 50051);
		if (args.length < 2) {
			java.io.Console console = System.console();
			username = console.readLine("Username: ");
			password = new String(console.readPassword("Password: "));
		} else {
			username = args[0];
			password = args[1];
		}
		client.login(username, password);
	}
}

