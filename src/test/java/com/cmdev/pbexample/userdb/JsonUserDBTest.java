package com.cmdev.pbexample.userdb;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.junit.*;

public class JsonUserDBTest {

    private JsonUserDB userDB;
    
    private static final String TEST_USER = "{\"arnold@goldsgym.com\": {"+
        "\"groups\": ["+
        "\"governator\","+
        "\"bodybuilder\""+
        "],"+
        "\"userName\": \"arnold\","+
        "\"userSurname\": \"Schwarzenegger\","+
        "\"userEmail\": \"arnold@goldsgym.com\","+
        "\"userCommonName\": \"Arnold Schwarzenegger\","+
        "\"userGivenName\": \"Arnold\""+
        "}"+
        "}";

    @Test
    public void testLookupUser() {
        try {
            this.userDB = new JsonUserDB(new StringReader(TEST_USER));
            User u = this.userDB.lookupUser("arnold@goldsgym.com");
            assertEquals("Wrong username", "arnold", u.userName);
            assertEquals("Wrong user email", "arnold@goldsgym.com", u.userEmail);
            List<String> grps = u.groups;
            assertArrayEquals("Groups mismatch", new String[] {"governator", "bodybuilder"}, grps.toArray());
            assertEquals("Wrong common name", "Arnold Schwarzenegger", u.userCommonName);
            assertEquals("Wrong given name", "Arnold", u.userGivenName);
            assertEquals("Wrong  surname", "Schwarzenegger", u.userSurname);
		} catch (IOException e) {
			fail(e.getMessage());
		}
    }
    
}